### What is this pipeline for? ###


* Overview

This pipeline was developed to process all the WGS/WES data sequenced using PCR-free protocol for the ADSP/GCAD project.  
This is  
       1) fully designed and optimized for large scale production of WGS/WES data on the Amazon cloud; 
       2) Developed in coordination with other NIH programs including TOPMed and the CCDG. 
Optionally, it can interactive with a tracking database that can display a variety of quality metrics instantly. This was tested with HiSeq 2000, HiSeq2500/HiSeq X data.

This pipeline takes an input BAM file and re-maps to the hg38 genome assembly using BWA. Supplementary alignments are retained for downstream structure variant optimization. It then performs duplicate marking; base recalibration, indel-realignment and quality score binning using GATK 3.7. Next, it performs SNV genotype calling using GATK. 


#### Workflow summary ####

 * Stage 0
   Convert BAM to fastq and 
   BWA mapping
   
    * S0a -Sort bam with read names 
           Sambamba sort
         
    * S0b -Roll back and Map reads to reference genome
           Picard RevertSam 
           Picard MarkIlluminaAdapters
           Picard SamtoFastq
           BWA-MEM –K 100000000 and –Y (TOPMed version)
          
 * Stage 1 
   Mark Duplicates
   Check quality of BAM 
   
    *  S1a -Mark duplicated reads
          Samtools sort
          Samblaster addmatetags (CCDG version)  
          Samtools sort 
          BamUtil  NonPrimaryDedup branch dedup_lowmem for mark  
          duplicates (TOPMed version) 
          
    *   S1b -Check data-quality of mapping (pre-VCF check)
          Sambamba flagstats (bam statistics and coverage) 
          vcftools verifyBamID (GWAS-bam concordance)
          
 * Stage 2 
   BAM Recalibration and Realignment 
   Calling individual VCFs from BAMs
   Check quality of VCFs
   Generate project level VCFs
   
    *  S2a -Recalibrate reads on BAMs
          GATK BaseRecalibrator
          GATK PrintReads 
          Sambamba merge
          GATK RealignTargetCreator
          GATK IndelRealigner
          
    *  S2b -Performing individual genotype call, generate gVCFs (GATK) 
          GATK HaplotypeCaller                      
          GATK VariantEval
          GATK GenotypeConcordance
          vcftools 
          
    *  S2c -Project level VCF
           Merge GATK gVCFs



### How do I get set up? ###

#### Software Dependencies ####
  
  [BWA][bwalink] (version 0.7.15) 
  [bwalink]: https://github.com/lh3/bwa/releases/tag/v0.7.15 
  
  [SAMTOOLS][samtoolslink] (version: 1.3.1-42-g0a15035(using htslib 1.3.2-135-g50db54b)
  [samtoolslink]: https://github.com/samtools/samtools/releases/tag/1.3.1
  
  [GATK][GATKlink] (version 3.7)
  [GATKlink]: https://software.broadinstitute.org/gatk/download/
  
  [PICARD][PICARDlink] (version 2.8.1)
  [PICARDlink]: https://github.com/broadinstitute/picard/releases/tag/2.8.1
  
  [SAMBABA][SAMBABAlink] (version v0.6.5)
  [SAMBABAlink]: https://github.com/broadinstitute/picard/releases/tag/2.8.1
  
  [SAMBLASTER][SAMBLASTERlink] (version 0.1.12b)
  [SAMBLASTERlink]: https://github.com/GregoryFaust/samblaster/releases/tag/v.0.1.24
  
  [VCFTOOLS][VCFTOOLSlink] (version 0.1.12b)
  [VCFTOOLSlink]: https://sourceforge.net/projects/vcftools/files/
  
  [BAMUTIL][BAMUTILlink] (version v1.0.13)
  [BAMUTILlink]: https://github.com/statgen/bamUtil/tree/NonPrimaryDedup
  
  [BCFTOOLS][BCFTOOLSlink] (version: 1.3.1-1)
  [BCFTOOLSlink]: https://github.com/samtools/bcftools/releases/tag/1.3.1
  

#### Hg38 Reference genome version

   This exact reference genome needs to be used. Description:
  
  *  GRCh38DH, 1000 Genomes Project version [GRCh38_full_analysis_set_plus_decoy_hla.fa](3366 contigs)
    *  md5sum: 64b32de2fc934679c16e83a2bc072064
         *  [hg38link](ftp://ftp.1000genomes.ebi.ac.uk/vol1/ftp/technical/reference/GRCh38_reference_genome/GRCh38_full_analysis_set_plus_decoy_hla.fa)
    *  Includes the standard set of chromosomes and alternate sequences from GRCh38
    *  Includes the decoy sequences
    *  Includes additional alternate versions of the HLA locus

#### Alignment method  
   Alignment strategy needs to be exactly the same
  
  * Aligner: BWA-MEM 
     * Version: 0.7.15  with `parameters –K 100000000 and –Y`

#### Dependency Files for GATK
* dbSNP annotation  
    * [dbSNP 138 lifted SNV file](https://storage.googleapis.com/genomics-public-data/resources/broad/hg38/v0/Homo_sapiens_assembly38.dbsnp138.vcf)
         * [Corresponding index file](https://storage.googleapis.com/genomics-public-data/resources/broad/hg38/v0/Homo_sapiens_assembly38.dbsnp138.vcf.idx)
* Indels annotation 
    * [dbSNP 138 lifted Indel file](https://storage.googleapis.com/genomics-public-data/resources/broad/hg38/v0/Homo_sapiens_assembly38.known_indels.vcf.gz)
        * [Corresponding index file](https://storage.googleapis.com/genomics-public-data/resources/broad/hg38/v0/Homo_sapiens_assembly38.known_indels.vcf.gz.tbi)
    * [Mills and 1000G gold standard indels file](https://storage.googleapis.com/genomics-public-data/resources/broad/hg38/v0/Mills_and_1000G_gold_standard.indels.hg38.vcf.gz)
        * [Corresponding index file](https://storage.googleapis.com/genomics-public-data/resources/broad/hg38/v0/Mills_and_1000G_gold_standard.indels.hg38.vcf.gz.tbi)

#### Setup for running the pipeline ####
    
    Amazon Machine Image:   [reference](http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/AMIs.html) 
           
        It includes all resources required to launch an instance, which is a virtual server in the cloud. It includes the following:
        * A template for the root volume for the instance
        * Launch permission that control which AWS accounts can use the AMI to launch instances
        * A block device that specifies the volumes to attach to the instance when it’s launched



* Configuration

  * AWS image ID – describe what is in this image ID 


      `
        MASTER_IMAGE_ID = ami-3f656129
      `

* AWS configure:
    You can set this up with the help of system administrator or the one who manages the AWS account.
   [reference](http://docs.aws.amazon.com/cli/latest/userguide/cli-chap-getting-started.html)

  * Starcluster configure file:
    http://star.mit.edu/cluster/docs/0.93.3/manual/configuration.html
    There are three required sections in starcluster configuration

    (1) AWS info [reference](http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/using-regions-availability-zones.html):
 
         AWS credentials:
          aws_access_key_ID
          aws_secret_access_ID
          Same credentials from AWS configure.
         Amazon EC2 Regions: 
          aws_region_name
          aws_region_host 
          

 
  Keypair: [reference](http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ec2-key-pairs.html)
  Define at least one keypair section that represents one of your keypairs on Amazon EC2.
  Key_location=/path/to/your/mykeypair.rsa


   (2) Cluster
       Define cluster template:
   
         In order to launch starcluster on EC2, you must provide a cluster template. 
         These templates are a collection of settings that define a single cluster configuration and are used when creating and configuring a cluster.
         Starcluster has a default template cluster smallcluster. 
         You can modify it according to your needs. You can also define multiple cluster templates, from which you can choose your template while launching the instance. 
         Below is an example of the "smallcluster template"
         

  
    # The section name is the name you give to your cluster template e.g.:
    [cluster smallcluster]
    keyname = mykeypair1 (Amazon EC2 keypair)
    # Number of ec2 instances to launch
    cluster_size = 
    # create the following user on the    cluster
    cluster_user = 
    # AMI for master node.
    master_image_id = 
    # Instance type for master node
    master_instance_type = 
    # AMI for worker nodes.
    node_image_id = 
    # Instance type for worker nodes.
    node_instance_type = 
    # Availability zone 
    availability_zone = 
    
  
     
* Tips 

    *  Choose instance that are optimized for memory intensive applications (e.g. i3.8xlarge, r3.8xlarge, d2.8xlarge) 
    *  Include the zone you want to run the pipeline. (eg. Zones a, b, d, e). Prices vary from zone to zone
  
  

#### Launching the pipeline
  * The pipeline can be launched by the following command line:
            
        Starcluster start –c clustertemplate –availability-zone *  –U pipelinescript –p Sample_ID 
        # clustertemplate:  
        smallcluster template
        # –availability-zone:  (check “availability_zone” above) 
        # pipelinescript: cmd_run_adsp_any_stage-single-bam-hg38.sh vs cmd_run_markdup_start-single-bam-hg38.sh (see “How to run the pipeline without linking to the database”) 
        # Sample_ID
        Sample name
  
  
 

#### How to run the pipeline without linking to the database

  (1)	If mapping to hg38 genome assembly is required, run this: 

         For BAM/CRAM: 
         Use “cmd_run_adsp_any_stage-single-bam-hg38.sh” 
         Note: this will roll back the mapped BAM to unmapped bam and starts the pipeline (i.e. including all stages 0, 1 and 2 of Table 1) 

  (2)	If mapping to hg38 genome assembly is done, and is using the exact same hg38 genome version (see Hg38 Reference genome version), then run the pipeline starting from the mark-duplication step: 

         Use “cmd_run_markdup_start-single-bam-hg38.sh”  to start from Mark Duplicate step (i.e. including stages 1 and 2 only of Table 1). 
