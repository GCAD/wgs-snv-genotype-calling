#!/bin/bash
#
# readname-sort.sh
# sort the bam by name(queryname) while filtering out read groups into a new bam
#
# @param $SEQFILE - input BAM
# @param $RG      - read-group for filtering by
# @return OUTFILE - single read-group BAM sorted by name
set -x
export SEQFILE=$1
export RG="$2"

source $DIR/lib/common_functions.sh

[[ -z "$DIR" ]] && echo "Missing \$DIR" && exit 100
[[ -z "$SEQFILE" ]] && echo "Missing \$SEQFILE" && exit 101
[[ -z "$RG" ]] && echo "Missing \$RG" && exit 102

[[ -z "$RESULTS_DIR" ]] && echo "Missing \$RESULTS_DIR" && exit 103
[[ -z "$SAMBAMBA" ]] && echo "Missing \$SAMBAMBA" && exit 104

[[ -z "$THREADS" ]] && echo "Missing \$THREADS, using default" && export THREADS=$MAX_THREADS

NOW=$(date '+%F+%T');
SRT=$(date +%s)
OUTFILE="$TMP_DIR/$RG.sorted-byname.bam"

ulimit -Hn 65536
ulimit -Sn 65536
ulimit -Sn

track_item rg_sort_start_tm $NOW
track_item current_operation "readname-sort"

if [ "$RG" == "A" ];then
  FILTER=""
else
  FILTER="--filter [RG]=="\'$RG\'
fi

echo  $SAMBAMBA sort \
           --tmpdir=$TMP_DIR \
           --nthreads $THREADS \
           --sort-by-name \
           $FILTER \
           --compression-level 8 \
           --out "$OUTFILE" \
             $SEQFILE

 $SAMBAMBA sort \
           $FILTER \
           --tmpdir=$TMP_DIR \
           --nthreads $THREADS \
           --sort-by-name \
           --compression-level 8 \
           --out "$OUTFILE" \
             $SEQFILE

ESTAT=$?
echo $ESTAT

if [ ! -s "$OUTFILE" ];then
  echoerr "Missing OUTPUT $OUTFILE"
  exit 100
fi

END=$(date +%s)
TM=$[$END-$SRT]

NOW=$(date '+%F+%T');
track_item rg_sort_end_tm $NOW
track_item rg_sort_duration $TM


exit $ESTAT
