#!/bin/bash -x
#
# streamBAM2bwa.sh
# uses SamToFastq to convert input BAM to FASTQ in stdout
# piped to BWA for mapping, output piped to split-sam-stream for
# splitting by chr
# @param SEQFILE - input BAM, single read-group, sorted by name, using original qualities
# @param THREADS - # of threads for BWA
source $DIR/lib/common_functions.sh

export SEQFILE="$1"
export THREADS=$2

trap "error-to-otto BWA-failed-$NM;track_item current_operation SamToFastq-BWA-ErrorState;exit 100" SIGSTOP SIGUSR1 SIGUSR2 SIGHUP SIGINT SIGTERM ERR

[[ -z "$SEQFILE" ]] && echo "Missing \$SEQFILE" && exit 100
[[ ! -s "$SEQFILE" ]] && echo "Empty \$SEQFILE" && track_item current_operation "SamToFastq-BWA-ErrorState" && exit 100
[[ -z "$TMP_DIR" ]] && echo "Missing \$TMP_DIR" && exit 101
[[ -z "$REF_FASTA" ]] && echo "Missing \$REF_FASTA" && exit 102
[[ -z "$SAMTOOLS" ]] && echo "Missing \$SAMTOOLS" && exit 103
[[ -z "$PICARD" ]] && echo "Missing \$PICARD" && exit 104
[[ -z "$BWA" ]] && echo "Missing \$BWA" && exit 105

export NM=$(basename $SEQFILE)
export NM=${NM/.bam/}
export NM=${NM/.sorted-byname/}

export RG_LINE=$($SAMTOOLS view -H $SEQFILE |grep  "^@RG" | grep -m 1 -P "ID:${NM}\s" |sed 's/\t/\\t/g')

mkdir -p $TMP_DIR/$NM/sam
OUTDIR=$TMP_DIR/$NM/sam

NOW=$(date '+%F+%T')
SRT=$(date +%s)
track_item streamBAM2bwa_start_tm $NOW
track_item current_operation "SamToFastq-BWA"

touch $OUTDIR/start
java -Xmx500M -jar $PICARD RevertSam \
  TMP_DIR=$TMP_DIR_R \
  OUTPUT_BY_READGROUP=false\
  RESTORE_ORIGINAL_QUALITIES=true\
  SORT_ORDER=queryname\
  COMPRESSION_LEVEL=0\
  VALIDATION_STRINGENCY=SILENT \
  QUIET=true \
  INPUT=$SEQFILE \
  OUTPUT=/dev/stdout | \
   java -Xmx500M -jar $PICARD MarkIlluminaAdapters \
    TMP_DIR=$TMP_DIR_R \
    METRICS=$LOGS/$NM.mia_metrics.o1 \
    COMPRESSION_LEVEL=0 \
    VALIDATION_STRINGENCY=SILENT \
    QUIET=true \
    INPUT=/dev/stdin \
    OUTPUT=/dev/stdout | \
      java -Xmx2G -jar $PICARD SamToFastq  \
         CLIPPING_ATTRIBUTE=XT \
         CLIPPING_ACTION=2 \
         INTERLEAVE=true \
         INCLUDE_NON_PF_READS=true \
         INCLUDE_NON_PRIMARY_ALIGNMENTS=false \
         TMP_DIR=$TMP_DIR_R \
         COMPRESSION_LEVEL=0 \
         VALIDATION_STRINGENCY=SILENT \
         QUIET=true \
         INPUT=/dev/stdin \
         FASTQ=/dev/stdout | \
            $BWA mem \
                 -t $THREADS \
                 -K 100000000 \
                 -Y \
                 -R "$RG_LINE" \
                 -p $REF_FASTA - | \
              $DIR/split-sam-stream.pl $OUTDIR/$NM.aligned && \
touch $OUTDIR/stop

ESTAT=$?
echo $ESTAT
echo "${PIPESTATUS[0]} ${PIPESTATUS[1]} ${PIPESTATUS[2]} ${PIPESTATUS[3]}"


find $OUTDIR -size 20c -delete

if [ ! -e "$OUTDIR/stop" ] || [ $ESTAT != 0 ] ||  [ $(ls $OUTDIR | wc -l) -lt 1 ];then
  echo "ERROR! Incomplete processing"
  error-to-otto "streamBAM2bwa failed $NM"
  track_item current_operation "streamBAM2bwa-ErrorState"
  exit 100
fi

NOW=$(date '+%F+%T')
END=$(date +%s)
TM=$[$END-$SRT]

track_item streamBAM2bwa_done_tm $NOW
track_item streamBAM2bwa_duration $TM


exit $ESTAT
