#!/bin/bash -x

source pipeline.ini
source lib/common_functions.sh
source lib/seqfile_functions.sh

while getopts dr:m: option
do
   case "$option" in
      d) DODEBUG=true;;       # option -d to print out commands
      r) RGBAM="$OPTARG";;    # full read-group bam path include BAM_DIR
      m) MRGBAM="$OPTARG";;   # merge bam name (output bam)
   esac
done

[[ -z "$RGBAM" ]] && echo "Missing \$RGBAM" && exit 100
[[ -z "$MRGBAM" ]] && echo "Missing \$MRGBAM" && exit 101
[[ -z "$BAM_DIR" ]] && echo "Missing \$BAM_DIR" && exit 102


export NM=${RGBAM/.bam/}
export NM=${NM/.fastq/}
export NM=${NM/.gz/}
export NM=$(basename $NM)

export TMP_DIR=$(get_tmp)

GRP_CT=$(readgroup_cnt $RGBAM)

# Get read groups as array
RGRPS=($(get_readgroups $RGBAM))

for RG in ${RGRPS[@]};do

   for CHR in `seq -f "chr%g" 1 22` chrX chrY chrM other unknown;do
        if [ ! -s "$TMP_DIR/$RG/sam/$RG.aligned.$CHR.sorted.bam" ];then

          MEM="9.6G"
          if [ "$CHR" == "chr1" ] || [ "$CHR" == "chr2" ] || [ "$CHR" == "chr3" ] || [ "$CHR" == "chr4" ] || [ "$CHR" == "chr5" ];then
            MEM="12G"
          fi
          qsub -N sortSam-$RG-$CHR \
           -hold_jid BAM2BWA-$RG \
           -j y \
           -o $LOGS/ \
           -cwd \
           -notify \
           -l h_vmem=$MEM \
           -V \
           $DIR/stage1/sortBam_v2.sh $TMP_DIR/$RG/sam/$RG.aligned.$CHR.sam.gz
       fi
   done

done

if [ ! -s "$MRGBAM" ];then
 MAX=$(get_max_mem)
 qsub -N markDupAll-$NM \
   -hold_jid "sortSam-*" \
   -j y \
   -o $LOGS/ \
   -cwd \
   -notify \
   -l h_vmem=$MAX \
   -V \
   stage1/markDupAllBam_v3.sh -r $RGBAM -o $MRGBAM
fi

for CHR in {1..22};do
 qsub -N "depth-$NM-chr$CHR" \
   -hold_jid "markDupAll-$NM" \
   -j y \
   -o $LOGS/ \
   -cwd \
   -l h_vmem=2G \
   -V \
   $DIR/stage1/depthOfCoverage.sh -i $MRGBAM -c "chr$CHR" -o "$RESULTS_DIR/$NM.depth.txt"
done

qsub -N "markCompletedDepth-${NM}" \
   -hold_jid "depth-$NM-*" \
   -j y \
   -o $LOGS/ \
   -cwd \
   -V \
   $DIR/stage1/markCompletedDepthOfCoverage.sh -i "$RESULTS_DIR/$NM.depth.txt" -b "$MRGBAM"

exit $?
