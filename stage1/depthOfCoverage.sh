#!/bin/bash -x
source $DIR/pipeline.ini
source $DIR/lib/common_functions.sh

while getopts di:c:o: option
do
   case "$option" in
      d) DODEBUG=true;;       # option -d to print out commands
      i) INPUTBAM="$OPTARG";;
      c) CHR="$OPTARG";;      # Regions
      o) OUTFILE="$OPTARG";;
   esac
done

[[ -z "$INPUTBAM" ]] && echo "Missing \$INPUTBAM" && exit 100
[[ ! -s "$INPUTBAM" ]] && echo "$INPUTBAM does not exist" && track_item current_operation "BaseRecalibrator-ErrorState" && exit 100
[[ -z "$CHR" ]] && echo "Missing \$CHR" && exit 100

NM=${INPUTBAM/.bam/}
NM=$(basename $NM)
NM=${NM/.sorted/}
NM=${NM/.dupmarked/}

SRT=$(date +%s)
NOW=$(date '+%F+%T')
track_item cal_depth_start_tm $NOW
track_item current_operation "DepthOfCoverage-$CHR"

$SAMBAMBA depth region \
  -t 2 \
  --combined \
  -L $CHR \
  -T 5 -T 10 -T 20 -T 30 -T 40 -T 50 \
  $INPUTBAM >> $OUTFILE

ESTAT=$?

END=$(date +%s)
TM=$[$END-$SRT];
NOW=$(date '+%F+%T')

echo $ESTAT
echo $TM

if [ "$ESTAT" == 0 ];then
   track_item cal_depth_done_time $NOW

else
  echoerr "Bad exit status: $ESTAT"
  track_item current_operation "DepthOfCoverage-ErrorState"
  exit 100
fi
