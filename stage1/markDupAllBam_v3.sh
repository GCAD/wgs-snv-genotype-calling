#!/bin/bash -x
#
# markDupAllBam.sh
# @param (-d) DODEBUG=true - not used
# @param (-r RGBAM) - full read-group bam path include BAM_DIR, used to grab read group list
# @param (-o MRGBAM) - merge bam name (output bam)
source $DIR/lib/common_functions.sh
source $DIR/lib/seqfile_functions.sh

while getopts dr:o: option
do
   case "$option" in
      d) DODEBUG=true;;       # option -d to print out commands
      r) RGBAM="$OPTARG";;    # full read-group bam path include BAM_DIR
      o) MRGBAM="$OPTARG";;   # merge bam name (output bam)
   esac
done

trap "error-to-otto MarkDup-Merge-failed-$NM;track_item current_operation MarkDup-Merge-ErrorState;exit 100" SIGSTOP SIGUSR1 SIGUSR2 SIGHUP SIGINT SIGTERM ERR

export NM=${RGBAM/.bam/}
export NM=$(basename $NM)

[[ -z $RGBAM ]] && echo "Missing \$RGBAM" && exit 100
[[ ! -s $RGBAM ]] && echoerr "Empty \$RGBAM" && exit 100
[[ -z $MRGBAM ]] && echo "Missing \$MRGBAM" && exit 100

TMP_DIR=$(get_tmp)
PREFIX=${MRGBAM/.bam/}
FPREFIX=$(basename $PREFIX)
GRP_CT=$(readgroup_cnt $RGBAM)

# Get read groups as array
RGRPS=($(get_readgroups $RGBAM))

INPUTS=""
for RG in ${RGRPS[@]};do
   for CHR in `seq -f "chr%g" 1 22` chrX chrY chrM other unknown;do
       if [ -s "$TMP_DIR/$RG/sam/$RG.aligned.$CHR.sorted.bam" ];then
          INPUTS+="$TMP_DIR/$RG/sam/$RG.aligned.$CHR.sorted.bam "
       else
          echoerr "Missing input $TMP_DIR/$RG/sam/$RG.aligned.$CHR.sorted.bam"
          exit 100
       fi
   done
done

[[ -z $INPUTS ]] && echo ${RGRPS[@]} && echo "Missing \$INPUTS" && exit 102

rm -f $TMP_DIR/*.sorted-byname.bam

# Increase open files limit
ulimit -Hn 65536
ulimit -Sn 65536
ulimit

SRT=$(date +%s)
NOW=$(date '+%F+%T')

track_item markdup_merge_start_tm $NOW
track_item current_operation "MarkDup-Merge"

#sambamba-merge [options] <output.bam> <input1.bam> <input2.bam> [...]
time $SAMBAMBA merge --compression-level=9 $TMP_DIR_R/$NM.merged.bam $INPUTS
time $BAMUTIL dedup_lowmem --allReadNames --in $TMP_DIR_R/$NM.merged.bam --out $MRGBAM --force --excludeFlags 0xB00

rm -fv $TMP_DIR_R/$NM.merged.bam
time $SAMBAMBA index $MRGBAM


ESTAT=$?
echo $ESTAT
echo "${PIPESTATUS[0]} ${PIPESTATUS[1]} ${PIPESTATUS[2]} ${PIPESTATUS[3]}"

END=$(date +%s)
TM=$[$END-$SRT];
NOW=$(date '+%F+%T')

echo $TM
echo $(qstat -j $JOB_ID|grep maxvmem)

track_item markdup_merge_done_tm $NOW
track_item markdup_merge_duration $TM

if [ -s "${PREFIX}.bam" ];then

     track_item current_operation "MarkDup-Merge-flagstat"

     $SAMBAMBA flagstat ${PREFIX}.bam > ${PREFIX}.flagstat \
     && track_item current_operation "MarkDup-Merge-Upload" \
     && aws s3 cp ${PREFIX}.flagstat $RESULTSPATH/${FPREFIX}.flagstat \
     && rm -fR $TMP_DIR/*/sam \
     && for RG in ${RGRPS[@]};do rm -vf "$TMP_DIR/$RG.bam";done

     # Extract flagstat stats
     TOT=$(grep "in total"     ${PREFIX}.flagstat|grep -Po "^\d+")
     SECD=$(grep "secondary" ${PREFIX}.flagstat|grep -Po "^\d+")
     SUPP=$(grep "supplementary" ${PREFIX}.flagstat|grep -Po "^\d+")
     DUPS=$(grep "duplicates"  ${PREFIX}.flagstat|grep -Po "^\d+")
     MAPPED=$(grep "mapped ("  ${PREFIX}.flagstat|grep -Po "^\d+")
     SINGLE=$(grep "singletons" ${PREFIX}.flagstat|grep -Po "^\d+")
     PAIRED=$(grep "properly paired" ${PREFIX}.flagstat|grep -Po "^\d+")

     DPCENT=$(echo $DUPS   | awk -v TOTAL=$TOT '{printf "%3.2f", ($1/TOTAL*100)}')
     MPCENT=$(echo $MAPPED | awk -v TOTAL=$TOT '{printf "%3.2f", ($1/TOTAL*100)}')
     PPCENT=$(echo $PAIRED | awk -v TOTAL=$TOT '{printf "%3.2f", ($1/TOTAL*100)}')
     SPCENT=$(echo $SINGLE | awk -v TOTAL=$TOT '{printf "%3.2f", ($1/TOTAL*100)}')

     # Upload flagstat stats
     track_item markdup_merge_reads $TOT
     track_item markdup_merge_dups $DUPS
     track_item markdup_merge_dups_pcent $DPCENT
     track_item markdup_merge_mapped_reads $MAPPED
     track_item markdup_merge_mapped_pcent $MPCENT
     track_item markdup_merge_paired_reads $PAIRED
     track_item markdup_merge_paired_pcent $PPCENT
     track_item markdup_merge_singletons   $SINGLE
     track_item markdup_merge_singletons_pcent $SPCENT

     track_item markdup_merge_supp_cnt $SUPP
     track_item markdup_merge_2ndary_cnt $SECD

     track_item markdup_merge_supp_dup_cnt $($SAMBAMBA view -c -F "supplementary and duplicate" ${PREFIX}.bam)
     track_item markdup_merge_unmapped_dup_cnt $($SAMBAMBA view -c -F "unmapped and duplicate" ${PREFIX}.bam)


     track_item markdup_merge_rg_cnt $(readgroup_cnt ${PREFIX}.bam)
     track_item markdup_merge_bam_size $(stat -c '%s' ${PREFIX}.bam)

fi
