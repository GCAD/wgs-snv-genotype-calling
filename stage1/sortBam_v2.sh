#!/bin/bash
#
# sortBam.sh
# sorts the input file by query-name using samtools,
# streammed to samblaster for adding in MC/MQ tags
# output coordinate sorted BAM with MC/MQ tags
# @param SAM - input SAM file
# @output BAM - coordinate-sorted BAM
set -euxo pipefail
source $DIR/pipeline.ini
source $DIR/lib/common_functions.sh

export SAM="$1"
[[ -z $SAM ]] && echo "Missing \$SAM" && track_item current_operation SortSam-ErrorState && exit 100
[[ ! -s $SAM ]] && echo "Empty \$SAM" && track_item current_operation SortSam-ErrorState && exit 100
[[ -z $REF_FASTA ]] && echo "Missing \$REF_FASTA" && track_item current_operation SortSam-ErrorState && exit 100

BAM=${SAM/.sam.gz/.sorted.bam}
TMP_DIR=$(get_tmp)

NOW=$(date '+%F+%T')
track_item sortsam_start_tm $NOW
track_item current_operation "SortSam"

NM=$(basename ${SAM/.sam.gz/})


trap "error-to-otto SortSam-failed-$NM;track_item current_operation SortSam-ErrorState;exit 100" SIGSTOP SIGUSR1 SIGUSR2 SIGHUP SIGINT SIGTERM ERR


# sort input SAM.GZ by name as required input for samblaster, output to co sorted BAM
$SAMTOOLS sort --threads 2 -l 0 -n -T "$TMP_DIR_R/$NM.1" --output-fmt SAM "$SAM" | \
  $SAMBLASTER --addMateTags --ignoreUnmated | \
    $SAMTOOLS sort -@2     -l 5    -T "$TMP_DIR_R/$NM.2" --output-fmt BAM -o "$BAM"


ESTAT=$?
echo "${PIPESTATUS[0]} ${PIPESTATUS[1]} ${PIPESTATUS[2]}"
echo $ESTAT

NOW=$(date '+%F+%T')
track_item sortsam_done_tm $NOW

if [ "$ESTAT" != 0 ];then
  echoerr "error in SortSam"
  track_item current_operation "SortSam-ErrorState"
  exit 100
fi


exit $ESTAT
