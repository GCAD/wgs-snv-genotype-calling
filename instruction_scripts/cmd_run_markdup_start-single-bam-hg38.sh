#!/bin/bash -x
echo "Begin script"
sleep 60  #for starting cluster

export DIR="/mnt/adsp"

# Get SGE ENV
source /etc/profile.d/sge.sh

umount -v /mnt

###
# get processing code from git repo
# directory must be empty
###
rm -fR $DIR
umount -v /mnt
mkdir -vp $DIR
git clone -b full_bam_amazon --single-branch git@bitbucket.org:ottov123/adsp-wgs-pipeline.git $DIR/


# Setup Globals
source $DIR/pipeline.ini #1
source $DIR/lib/common_functions.sh
source $DIR/lib/seqfile_functions.sh

# Set Globals
export SM=$(get_sample_name)
export PRJ_ID=$(get_project_id)
export FSIZE=$(get_sample_attr orig_file_size $PRJ_ID $SM)
if [ -z "$SM" ] || [ -z "$PRJ_ID" ];then
 error-to-otto "Empty PRJ_ID or SM"
 echoerr "Empty PRJ_ID or SM"
 instant_shutdown
 exit 1
fi

export RID=$(init_run $PRJ_ID $SM)
if [ -z "$RID" ];then
 error-to-otto "Empty RID:$SM"
 echoerr "Empty RID"
 instant_shutdown
 exit 1
fi

get_instance_id && echo "\$IID:=$IID"
tag_instance
track_item instance_start_tm $(date '+%F+%T');
track_item current_operation "setup"
track_item stage0_instance_type $(get_instance_type)
track_item aws_processing_instance_id_stage0 $IID


if [[ $(get_instance_type) != r4* ]];then
  # setup hard drives
  $DIR/lib/setup_mounts.sh # provide $DIR/bam/, $DIR/results/
else
  make_ebs_drive $FSIZE 5
  git clone -b full_bam_amazon --single-branch git@bitbucket.org:ottov123/adsp-wgs-pipeline.git $DIR/
fi

setup_dirs

cd $DIR

## grab BAM
export S3PATH=$(get_s3_seqfile_location $PRJ_ID $SM) && echo "\$S3PATH:=$S3PATH"
export RESULTSPATH=$(get_s3_results_location $PRJ_ID $SM) && echo "\$RESULTSPATH:=$RESULTSPATH"

if [ -z $S3PATH ] || [ -z $RESULTSPATH ];then
  echo "Missing data paths $S3PATH $RESULTSPATH .";
  error-to-otto "missing data paths $S3PATH $RESULTSPATH ."
  track_item current_operation "missing-s3-download-location"
  instant_shutdown
  exit 1
fi

SRT=$(date +%s)

SEQFILE=$(basename $S3PATH)

if [ ! -s "$BAM_DIR/$SEQFILE" ];then
    echo "Downloading $BAM_DIR/$SEQFILE from $S3PATH"
    echo "aws s3 cp $S3PATH $BAM_DIR/$SEQFILE"

    track_item current_operation "s3-download"

    aws s3 cp --quiet --only-show-errors "$S3PATH" "$BAM_DIR/$SEQFILE"
    aws s3 cp "${S3PATH}.bai" "$BAM_DIR/${SEQFILE}.bai"

    if [ ! -s $BAM_DIR/$SEQFILE ];then
        #partial download, timeout
        error-to-otto "missing download file for $IID;$SM"
        track_item current_operation "s3-re-download"
        aws s3 cp --quiet --only-show-errors "$S3PATH" "$BAM_DIR/$SEQFILE"
        aws s3 cp "${S3PATH}.bai" "$BAM_DIR/${SEQFILE}.bai"


    fi
    if [ ! -s $BAM_DIR/$SEQFILE ];then
        #partial download, timeout
        track_item current_operation "ERROR-s3-download"
        error-to-otto "missing retry download file for $IID;$SM"
        instant_shutdown
    fi

    if [ "$(isCRAM $BAM_DIR/$SEQFILE)" == "1" ];then
       track_item current_operation "CRAM2BAM"
       CRAM2BAM "$BAM_DIR/$SEQFILE" "$BAM_DIR/$SM.bam" $REF_FASTA
       rm -v "$BAM_DIR/$SEQFILE"
    else
       mv -v "$BAM_DIR/$SEQFILE" "$BAM_DIR/$SM.bam"
    fi

    if [ "$(isSorted $BAM_DIR/$SM.bam)" == "0" ];then
      ulimit -Hn 65536
      ulimit -Sn 65536
      track_item current_operation "Sort-downloaded-file"
       $SAMBAMBA sort --tmpdir=$TMP_DIR $BAM_DIR/$SM.bam && \
          mv -v $BAM_DIR/$SM.sorted.bam $BAM_DIR/$SM.bam
    fi

    track_item current_operation "Check-downloaded-file"
    set +x
    [[ $(check_seqfile $BAM_DIR/$SM.bam) -eq 0 ]] && error-to-otto "seqfile currupted" && instant_shutdown
    set -x
    END=$(date +%s)
    TM=$[$END-$SRT];echo $TM

    track_item bam_s3_copy_to_ebs_stage0 $TM

    # check for @RG presense
    ZCT=$(readgroup_cnt $BAM_DIR/$SM.bam)
    if [ "$ZCT" -eq 0 ];then
       time add_generic_read_group $BAM_DIR/$SM.bam
    fi
fi

#stage 1
echo "Starting at MARKDUP"
make_new_tmp
export TMP_DIR=$(get_tmp)
LVL1BAM=$TMP_DIR/$SM.sorted.dupmarked.bam

$DIR/stage1/markDup-start.sh -i $BAM_DIR/$SM.bam -o $LVL1BAM

for CHR in {1..22};do
 qsub -N "depth-$SM-chr$CHR" \
   -hold_jid "markDupAll-$NM" \
   -j y \
   -o $LOGS/ \
   -cwd \
   -l h_vmem=2G \
   -V \
   $DIR/stage1/depthOfCoverage.sh -i $LVL1BAM -c "chr$CHR" -o $RESULTS_DIR/$SM.depth.txt
done

qsub -N "markCompletedDepth-$NM" \
   -hold_jid "depth-$SM-*" \
   -j y \
   -o $LOGS/ \
   -cwd \
   -V \
   $DIR/stage1/markCompletedDepthOfCoverage.sh -i $RESULTS_DIR/$SM.depth.txt -b "$LVL1BAM"



# stage 2a
# BaseRecalibrator + RealignTargetCreator + IndelRealigner
BQSRTBL="$RESULTS_DIR/${SM}.recal_data.table"
INDELINTRVLS="$RESULTS_DIR/indels.$SM.intervals"
LVL2BAM="${RESULTS_DIR}/${SM}.hg38.realign.bqsr.bam"
if [ $(ls -lh $BAM_DIR/$SM.bam|awk '{printf "%3.0f\n", $5 * 2}') -gt 300 ];then
  export TMP_DIR=$(get_tmp)
  LVL2BAM="${TMP_DIR}/${SM}.hg38.realign.bqsr.bam"
fi

$DIR/stage2a/submit-stage2a-bcal.sh   -h "markDupAll-$SM" -i "$LVL1BAM" -o "$BQSRTBL" # BCal.${SM}
$DIR/stage2a/submit-rtc.sh            -h "BCal.$SM"       -i "$LVL1BAM" -o "$INDELINTRVLS" # RTC.$SM
$DIR/stage2a/submit-indelRealigner.sh -h "RTC.$SM"        -i "$LVL1BAM" -r "$INDELINTRVLS" -b "$BQSRTBL" -o "$LVL2BAM" #IndelRealign-merge.${SM}

#
# stage 2b - HC only
$DIR/stage2b/submit-hc-full-bam-single-chr.sh -h "IndelRealign-merge.$SM" -i $LVL2BAM # HC.${SM}-chr$CHR

$DIR/stage2b/submit-variantEval.sh -h "HC.${SM}-*"

process_shutdown
#end
