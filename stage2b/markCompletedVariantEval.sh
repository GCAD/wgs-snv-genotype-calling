#!/bin/bash -x

source $DIR/lib/common_functions.sh

while getopts di: option
do
   case "$option" in
      d) DODEBUG=true;;        # option -d to print out commands
      i) INFILE="$OPTARG";;    # variant eval output
   esac
done

[[ -z $INFILE ]] && echoerr "Missing \$INFILE" && exit 100
[[ ! -s $INFILE ]] && echoerr "\$INFILE does not exist" && exit 100
[[ -z $SM ]] && echoerr "Missing \$SM" && exit 100

[[ -z $DODEBUG ]] && track_item current_operation "VariantEval-stats-upload"

NSNPS=$(grep "VariantSummary.*all" $INFILE | awk '{print $8}' )
track_item variantEval_nSNPs $NSNPS

TITV=$(grep "VariantSummary.*all" $INFILE | awk '{print $9}' )
track_item variantEval_TiTv $TITV

NINDEL=$(grep "VariantSummary.*all" $INFILE | awk '{print $14}' )
track_item variantEval_nIndel $NINDEL

TOTAL_SITES=$(grep "CountVariants.*all " $INFILE | awk '{print $6}' )
track_item variantEval_totSites $TOTAL_SITES

TOTAL_VARIANTS=$(grep "CountVariants.*all " $INFILE | awk '{print $9}' )
track_item variantEval_totVariants $TOTAL_VARIANTS

KNOWN_SITES=$(grep "CountVariants.*known " $INFILE | awk '{print $9}' )
track_item variantEval_knSites $KNOWN_SITES


RATIO1=$(echo $KNOWN_SITES|awk -v tv=$TOTAL_VARIANTS '{print (1-($1/tv))*100}')
track_item novel_over_total_variant_perc $RATIO1

TITV_K=$(grep "TiTvVariantEvaluator.*known" $INFILE | awk '{print $8}')
track_item variantEval_known_TiTv $TITV_K

TITV_N=$(grep "TiTvVariantEvaluator.*novel " $INFILE | awk '{print $8}')
track_item variantEval_novel_TiTv $TITV_N


NTI_T=$(grep "TiTvVariantEvaluator.*all" $INFILE | awk '{print $6}')
track_item variantEval_total_nTi $NTI_T

NTI_K=$(grep "TiTvVariantEvaluator.*known" $INFILE | awk '{print $6}')
track_item variantEval_known_nTi $NTI_K

NTI_N=$(grep "TiTvVariantEvaluator.*novel " $INFILE | awk '{print $6}')
track_item variantEval_novel_nTi $NTI_N


NTV_T=$(grep "TiTvVariantEvaluator.*all" $INFILE | awk '{print $7}')
track_item variantEval_total_nTv $NTV_T

NTV_K=$(grep "TiTvVariantEvaluator.*known " $INFILE | awk '{print $7}')
track_item variantEval_known_nTv $NTV_K

NTV_N=$(grep "TiTvVariantEvaluator.*novel " $INFILE | awk '{print $7}')
track_item variantEval_novel_nTv $NTV_N



if [ ! $DODEBUG ];then

  if [ -z $RESULTSPATH ];then
     export RESULTSPATH=$(get_s3_results_location $PRJ_ID $SM)
  fi

  if [ ! -z $RESULTSPATH ];then
     echo "uploading eval file"
     find $LOGS -size 0 -delete # delete empty files

     F=$(basename $INFILE)
     aws s3 cp "$INFILE" "$RESULTSPATH/VCF/GATK/$F"

     echo "uploading pipeline logs"
     for FLOG in ${LOGS}/*.o[0-9]*;do
       V=$(basename $FLOG)
       aws s3 cp "$FLOG" "$RESULTSPATH/LOGS/$V"
     done

  else
    echoerr "Not uploading, no \$RESULTSPATH"
    track_item current_operation "VariantEval-upload-ErrorState"
    exit 100
  fi

  FILECOUNT=$(aws s3 ls --recursive $RESULTSPATH | wc -l)
  track_item current_operation "completed-$FILECOUNT-files"

fi

