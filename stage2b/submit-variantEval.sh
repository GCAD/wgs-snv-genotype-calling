#!/bin/bash

source $DIR/pipeline.ini #SM, LSAC, URI, PRJ_ID,BAM,RESULTS_DIR from ENV
source $DIR/lib/common_functions.sh

while getopts dh: option
do
   case "$option" in
      d) DODEBUG=true;;               # option -d to print out commands
      h) HJID="-hold_jid $OPTARG";;    # job id/name for hold_jid
   esac
done


[[ -z $SM ]] && echoerr "Error: Missing SM $SM" && exit 100

QSUB=qsub
if [ $DODEBUG ];then
 QSUB="echo qsub"
fi

OUTFILE="$RESULTS_DIR/eval.$SM.txt"
PREFIX="$RESULTS_DIR/$SM.raw_variants"
THREADS=7

MEM=$(adjustWorkingMem 42G $THREADS)

$QSUB -pe DJ $THREADS \
      -l h_vmem=$MEM \
      -N "VariantEval.${SM}" \
      -o $LOGS \
      -V \
      $HJID \
      -j y \
       $DIR/stage2b/variantEval.sh -p "$PREFIX" -t $THREADS -o "$OUTFILE"


$QSUB -N MarkCompleteVariantEval-$SM \
  -j y \
  -o $LOGS \
  -hold_jid "VariantEval.${SM}" \
  -cwd \
  -V \
  $DIR/stage2b/markCompletedVariantEval.sh -i "$OUTFILE"
