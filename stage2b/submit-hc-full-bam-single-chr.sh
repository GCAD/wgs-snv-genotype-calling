#!/bin/bash

source $DIR/pipeline.ini #SM, LSAC, URI, PRJ_ID,BAM,RESULTS_DIR from ENV
source $DIR/lib/common_functions.sh

while getopts dh:i: option
do
   case "$option" in
      d) DODEBUG=true;;               # option -d to print out commands
      h) HJID="-hold_jid $OPTARG";;    # job id/name for hold_jid
      i) INPUTBAM="$OPTARG";;
   esac
done


[[ -z $SM ]] && echoerr "Error: Missing SM $SM" && exit 100
[[ -z $INPUTBAM ]] && echoerr "Error: Missing BAM $BAM" && track_item current_operation "HaplotypeCaller-ErrorState" && exit 100

QSUB=qsub
if [ $DODEBUG ];then
 QSUB="echo qsub"
fi

# get isPCR_free attribute from the database
if [ "$(get_sample_attr isPCR_free)" == "1" ];then
 PCR="-n"
fi

PREFIX=$RESULTS_DIR/$SM.raw_variants
RMEM="23.5G"
export XMEM="17g"

    for CHR in {1..25};do
        THREADS=4
        PRI=100

        if [ $CHR -eq 1 ] || [ $CHR -eq 2 ] || [ $CHR -eq 12 ] || [ $CHR -eq 6 ];then
            THREADS=4
            PRI=900
        elif [ $CHR -eq 3 ] || [ $CHR -eq 5 ] || [ $CHR -eq 13 ];then
            THREADS=3
            PRI=900
        elif [ $CHR -eq 9 ];then
            THREADS=6
            PRI=900
        elif [ $CHR -eq 14 ];then
            THREADS=1
            PRI=900
        fi

        if [ $CHR -eq 17 ];then
            THREADS=3
            PRI=800
        elif [ $CHR -eq 11 ];then
            THREADS=2
            PRI=750
        elif [ $CHR -eq 18 ];then
            THREADS=3
            PRI=700
        fi

        if [ $CHR -eq 16 ];then
            THREADS=4
            PRI=650
        elif [ $CHR -eq 10 ];then
            THREADS=3
            PRI=600
        elif [ $CHR -eq 19 ];then
            THREADS=1
            PRI=550
        elif [ $CHR -eq 8 ];then
            THREADS=2
            PRI=500
        elif [ $CHR -eq 22 ];then
            THREADS=1
            PRI=450
        elif [ $CHR -eq 15 ];then
            THREADS=2
            PRI=400
        elif [ $CHR -eq 21 ];then
            THREADS=1
            PRI=350
        elif [ $CHR -eq 4 ];then
            THREADS=4
            PRI=300
        elif [ $CHR -eq 20 ];then
            THREADS=3
            PRI=250
        elif [ $CHR -eq 7 ];then
            THREADS=3
            PRI=200
        fi

        if [ $CHR -eq 23 ];then
            THREADS=5
            CHR="X"
            PRI=100
        elif [ $CHR -eq 24 ];then
            CHR="Y"
            PRI=100
            RMEM="27G"
            export XMEM="21g"
        elif [ $CHR -eq 25 ];then
            THREADS=1
            CHR="M"
            PRI=100
            RMEM="31G"
            export XMEM="25g"
        fi

        MEM=$(adjustWorkingMem $RMEM $THREADS)
        $QSUB -pe DJ $THREADS \
            -l h_vmem=$MEM \
            -N "HC.${SM}-chr$CHR" \
            -p $PRI \
            -o $LOGS \
            -V \
            $HJID \
            -j y \
            $DIR/stage2b/haplotyper-single-chromosome.sh -c "chr$CHR" -p "$PREFIX" -t $THREADS -i "$INPUTBAM" $PCR

    done

$QSUB -N MarkCompleteHC-$SM \
  -j y \
  -o $LOGS \
  -hold_jid "HC.${SM}-*" \
  -cwd \
  -V \
  $DIR/stage2b/markCompletedHC.sh -p "$PREFIX"
