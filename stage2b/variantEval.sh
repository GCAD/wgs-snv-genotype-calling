#!/bin/bash

source $DIR/pipeline.ini #REF_FASTA, GATK, DBSNP, LOGS from ENV
source $DIR/lib/common_functions.sh

while getopts dt:p:o: option
do
   case "$option" in
      d) DODEBUG=true;;               # option -d to print out commands
      t) THREADS="$OPTARG";;
      p) PREFIX="$OPTARG";;
      o) OUTFILE="$OPTARG";;
   esac
done

[[ -z $PREFIX ]] && echoerr "Error: \$PREFIX Missing" && track_item current_operation "HaplotypeCaller-ErrorState" && exit 100
[[ -z $OUTFILE ]] && echoerr "Error: \$OUTFILE Missing" && track_item current_operation "HaplotypeCaller-ErrorState" && exit 100
[[ -z $GATK ]] && echoerr "Error: \$GATK Missing" && track_item current_operation "HaplotypeCaller-ErrorState" && exit 100
[[ -z $DBSNP ]] && echoerr "Error: \$DBSNP Missing" && track_item current_operation "HaplotypeCaller-ErrorState" && exit 100
[[ -z $LOGS ]] && echoerr "Error: \$LOGS Missing" && track_item current_operation "HaplotypeCaller-ErrorState" && exit 100
[[ -z $THREADS ]] && echoerr "Missing THREADS" && THREADS=7

#PREFIX=$RESULTS_DIR/$SM.raw_variants

track_item current_operation VariantEval

for CHR in `seq -s" " 1 22` X Y M;do
  if [ ! -s "$PREFIX.chr$CHR.g.vcf.gz" ];then
    echoerr "Missing input file $PREFIX.chr$CHR.g.vcf.gz"
    track_item current_operation "VariantEval-ErrorState"
    exit 100
  fi

  MSTR+="--eval $PREFIX.chr$CHR.g.vcf.gz "

done

SRT=$(date +%s)
NOW=$(date '+%F+%T')
track_item variant_eval_st_tm $NOW

java -jar $GATK \
   -T VariantEval \
   -R $REF_FASTA \
   --dbsnp $DBSNP \
   --goldStandard $GOLD \
   -EV CountVariants \
   -EV VariantSummary \
   -nt $THREADS \
    $MSTR \
   --mergeEvals \
   -o "$OUTFILE"

ESTAT=$?

echo $ESTAT

if [ ! -s $OUTFILE ] || [ $ESTAT != 0 ];then
  echoerr "Error: VariantEval failed exit $ESTAT"
  track_item current_operation "VariantEval-ErrorState"
  exit 100
fi

END=$(date +%s)
TM=$[$END-$SRT]

NOW=$(date '+%F+%T')
track_item variant_eval_dn_tm $NOW
track_item variant_eval_duration $TM

exit $ESTAT
