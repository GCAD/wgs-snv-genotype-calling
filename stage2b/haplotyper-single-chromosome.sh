#!/bin/bash
#
# haplotyper-single-chromosome.sh
# run GATK HaplotypeCaller on a region of input bam to generate a GVCF
#
# @param (-i INPUTBAM) - input SEQ FILE
# @param (-c CHR)      - region for "-L" interval flag
# @param (-p PREFIX)   - out-file naming prefix
# @param (-n)          - override PCRMODEL from CONSERVATIVE to NONE, required for PCR-free data
# @return OUTFILE - named as ${PREFIX}.$CHR.g.vcf.gz
set -x
source $DIR/pipeline.ini #REF_FASTA, GATK, DBSNP, LOGS from ENV
source $DIR/lib/common_functions.sh

while getopts dnt:i:p:c: option
do
   case "$option" in
      d) DODEBUG=true;;      # option -d to print out commands
      t) THREADS="$OPTARG";;
      i) INPUTBAM="$OPTARG";;
      p) PREFIX="$OPTARG";;
      c) CHR="$OPTARG";;
      n) PCRMODEL="NONE";;   # override PCRMODEL to NONE
   esac
done

[[ -z $INPUTBAM ]] && echoerr "Error: Missing \$INPUTBAM" && track_item current_operation "HaplotypeCaller-ErrorState" && exit 100
[[ ! -s $INPUTBAM ]] && echoerr "Error: INPUT $INPUTBAM does not exist" && track_item current_operation "HaplotypeCaller-ErrorState" && exit 100

[[ -z $CHR ]] && echoerr "Error: \$CHR Missing" && track_item current_operation "HaplotypeCaller-ErrorState" && exit 100
[[ -z $PREFIX ]] && echoerr "Error: \$PREFIX Missing" && track_item current_operation "HaplotypeCaller-ErrorState" && exit 100
[[ -z $GATK ]] && echoerr "Error: \$GATK Missing" && track_item current_operation "HaplotypeCaller-ErrorState" && exit 100
[[ -z $DBSNP ]] && echoerr "Error: \$DBSNP Missing" && track_item current_operation "HaplotypeCaller-ErrorState" && exit 100
[[ -z $LOGS ]] && echoerr "Error: \$LOGS Missing" && track_item current_operation "HaplotypeCaller-ErrorState" && exit 100
[[ -z $THREADS ]] && echoerr "Missing THREADS" && THREADS=$MAX_THREADS

: ${PCRMODEL:=CONSERVATIVE} # set default PCRMODEL to CONSERVATIVE, overriden by getopts
: ${XMEM:=17g}

#PREFIX=$RESULTS_DIR/$SM.raw_variants
TMP_DIR=$(get_tmp)

track_item current_operation HaplotypeCaller-$CHR

 java -Xmx$XMEM -Djava.io.tmpdir=$TMP_DIR \
      -jar $GATK \
          -T HaplotypeCaller \
          -R $REF_FASTA \
          -I "$INPUTBAM" \
          -nct $THREADS \
          -L $CHR  \
          --dbsnp $DBSNP \
          --genotyping_mode DISCOVERY \
          --minPruning 2 \
          -newQual \
          -stand_call_conf 30 \
          --emitRefConfidence GVCF \
          --pcr_indel_model $PCRMODEL \
          -l INFO \
          -log $LOGS/vch.$CHR.log \
          -o "${PREFIX}.$CHR.g.vcf.gz"

ESTAT=$?

echo $ESTAT

track_item current_operation HaplotypeCaller-${CHR}-exit

if [ $ESTAT != 0 ];then
  echoerr "Error: HaplotypeCaller failed exit $ESTAT"
  error-to-otto "Error: HaplotypeCaller-$CHR-$SM"
  track_item current_operation "HaplotypeCaller-ErrorState"
  exit 100
fi

exit $ESTAT
