#!/bin/bash -x

source $DIR/lib/common_functions.sh

while getopts dh:i:r:o: option
do
   case "$option" in
      d) DODEBUG=true;;               # option -d to print out commands
      h) HJID="-hold_jid $OPTARG";;    # job id/name for hold_jid
      i) INPUTBAM="$OPTARG";;
      o) OUTFILE="$OPTARG";;
      t) THREADS="$OPTARG";;
   esac
done

[[ -z "$INPUTBAM" ]] && echo "Missing \$INPUTBAM" && exit
[[ -z "$OUTFILE" ]] && echo "Missing \$OUTFILE" && exit
[[ -z "$THREADS" ]] && echo "Missing \$THREADS" && THREADS=$MAX_THREADS

NM=$(basename $INPUTBAM)
NM=${NM%%.*}

QSUB=qsub
if [ $DODEBUG ];then
 QSUB="echo qsub"
fi

MAX=$(get_max_mem)
MEM=$(adjustWorkingMem $MAX $THREADS)

$QSUB \
  -pe DJ $THREADS \
  -N "RTC.${SM}" \
   -j y \
   $HJID \
   -o $LOGS \
   -cwd \
   -V \
   -l h_vmem=$MEM \
    $DIR/stage2a/rtc.sh -i "$INPUTBAM" -o "$OUTFILE" -t "$THREADS"
