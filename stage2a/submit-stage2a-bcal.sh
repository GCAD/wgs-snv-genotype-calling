#!/bin/bash -x

source $DIR/lib/common_functions.sh

while getopts dh:i:o:t: option
do
   case "$option" in
      d) DODEBUG=true;;               # option -d to print out commands
      h) HJID="-hold_jid $OPTARG";;    # job id/name for hold_jid
      i) INPUTBAM="$OPTARG";;
      o) RTBLOUT="$OPTARG";;
      t) THREADS="$OPTARG";;
   esac
done

[[ -z "$THREADS" ]] && echo "Missing \$THREADS" && THREADS=$MAX_THREADS
[[ -z "$TMP_DIR" ]] && echo "Missing \$TMP_DIR" && exit 100
[[ -z "$KNOWN" ]] && echo "Missing \$KNOWN" && exit 101
[[ -z "$DBSNP" ]] && echo "Missing \$DBSNP" && exit 102

NM=$(basename $INPUTBAM)
NM=${NM%%.*}

QSUB=qsub
if [ $DODEBUG ];then
 QSUB="echo qsub"
fi

MAX=$(get_max_mem)
MEM=$(adjustWorkingMem $MAX $THREADS)

mkdir -p $(dirname $RTBLOUT)

$QSUB \
  -pe DJ $THREADS \
  -N BCal.${NM} \
   -terse \
   -j y \
   $HJID \
   -o $LOGS \
   -cwd \
   -V \
   -l h_vmem=$MEM \
    $DIR/stage2a/bcal.sh $INPUTBAM $RTBLOUT
