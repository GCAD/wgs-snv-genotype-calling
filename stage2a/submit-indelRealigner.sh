#!/bin/bash -x
source $DIR/lib/common_functions.sh

while getopts dh:i:r:b:o: option
do
   case "$option" in
      d) DODEBUG=true;;             # option -d to print out commands
      h) HJID="-hold_jid $OPTARG";; # job id/name for hold_jid
      i) INPUTBAM="$OPTARG";;
      r) REGIONS="$OPTARG";;        # option -r required target regions from RealignTargetCreator
      b) BQSR="$OPTARG";;           # optional -b for base recalibartion table from BaseRecalibrator
      o) OUTBAM="$OPTARG";;
   esac
done

[[ -z "$SM" ]] && echo "Missing \$SM" && exit 100
[[ -z "$INPUTBAM" ]] && echo "Missing \$INPUTBAM" && exit 100
[[ -z "$REGIONS" ]] && echo "Missing \$REGIONS" && exit 100
[[ -z "$OUTBAM" ]] && echo "Missing \$OUTBAM" && exit 100
[[ -z $THREADS ]] && export THREADS=$MAX_THREADS

QSUB=qsub
if [ $DODEBUG ];then
 QSUB="echo qsub"
fi

if [ ! -z "$BQSR" ];then
 BARGS="-b $BQSR"
fi

make_new_tmp
export TMP_DIR=$(get_tmp)

ARGS1="-j y \
   -o $LOGS \
   -V \
   -notify \
   $HJID \
   -cwd"


for CHR in {1..25};do

        if [ $CHR -eq 23 ];then
            CHR="X"
        elif [ $CHR -eq 24 ];then
            CHR="Y"
        elif [ $CHR -eq 25 ];then
            CHR="M"
        fi

        $QSUB -N "IndelRealign.${SM}-chr$CHR" \
              -l h_vmem=17G \
              $ARGS1 \
                $DIR/stage2a/indelrealigner.sh -i "$INPUTBAM" -c "chr$CHR" -r "$REGIONS" $BARGS -o "$TMP_DIR/$SM.indelrealign.chr$CHR.bam"


done

# IndelRealign unmapped reads, -L unmapped
# gotta have that slice
$QSUB -N "IndelRealign.${SM}-unmapped" \
    -l h_vmem=17G \
    $ARGS1 \
      $DIR/stage2a/indelrealigner.sh -i "$INPUTBAM" -c "unmapped" -r "$REGIONS" $BARGS -o "$TMP_DIR/$SM.indelrealign.unmapped.bam"

# for other contigs, use -XL flag to *exclude* chr1..22, chrX, chrM, unmapped
$QSUB -N "IndelRealign.${SM}-other" \
   -l h_vmem=17.5G \
   $ARGS1 \
     $DIR/stage2a/indelrealigner.sh -i "$INPUTBAM" -c "other" -r "$REGIONS" $BARGS -o "$TMP_DIR/$SM.indelrealign.other.bam"

MAX=$(get_max_mem)

MEM=$(adjustWorkingMem $MAX $THREADS)

$QSUB \
  -N IndelRealign-merge.${SM} \
   -j y \
   -o $LOGS \
   -V \
   -pe DJ $THREADS \
   -cwd \
   -notify \
   -hold_jid "IndelRealign.${SM}-*" \
   -l h_vmem=$MEM \
     $DIR/stage2a/indelrealigner-merge.sh -t $THREADS -p "$TMP_DIR/$SM.indelrealign" -o "$OUTBAM"
