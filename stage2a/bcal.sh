#!/bin/bash -x
source $DIR/pipeline.ini
source $DIR/lib/common_functions.sh

export INPUTBAM=$1
export RTBLOUT=$2 #${RESULTS_DIR}/bam/${SM}.recal_data.table

[[ -z "$INPUTBAM" ]] && echo "Missing \$INPUTBAM" && exit 100
[[ ! -s "$INPUTBAM" ]] && echo "$INPUTBAM does not exist" && track_item current_operation "BaseRecalibrator-ErrorState" && exit 100
[[ -z "$RTBLOUT" ]] && echo "Missing \$RTBLOUT" && track_item current_operation "BaseRecalibrator-ErrorState" && exit 100
[[ -z "$TMP_DIR" ]] && echo "Missing \$TMP_DIR" && exit 102
[[ -z "$KNOWN" ]] && echo "Missing \$KNOWN" && exit 103
[[ -z "$DBSNP" ]] && echo "Missing \$DBSNP" && exit 104
[[ -z "$THREADS" ]] && echo "Missing \$THREADS" && THREADS=$MAX_THREADS

NM=${INPUTBAM/.bam/}
NM=$(basename $NM)
NM=${NM/.sorted/}
NM=${NM/.dupmarked/}


mkdir -p $(dirname $RTBLOUT)

SRT=$(date +%s)
NOW=$(date '+%F+%T')
track_item bcal_table_start_tm $NOW
track_item current_operation "BaseRecalibrator"

java -Djava.io.tmpdir=$TMP_DIR \
     -jar $GATK \
           -T BaseRecalibrator \
           -R $REF_FASTA  \
           -I $INPUTBAM \
           -nct $THREADS  \
           --useOriginalQualities \
           -knownSites $KNOWN \
           -knownSites $DBSNP \
           -knownSites $GOLD \
             -o $RTBLOUT

ESTAT=$?

END=$(date +%s)
TM=$[$END-$SRT];
NOW=$(date '+%F+%T')



if [ "$ESTAT" == 0 ];then
   track_item bcal_duration $TM
   track_item bcal_done_time $NOW

   if [ -z "$RESULTSPATH" ];then
     export RESULTSPATH=$(get_s3_results_location $PRJ_ID $SM)
   fi
   if [ ! -z "$RESULTSPATH" ];then
      TBL=$(basename $RTBLOUT)

      aws s3 cp --storage-class STANDARD_IA $RTBLOUT $RESULTSPATH/$TBL
      track_item gatk_bqsr_recal_data_table_path $(rawurlencode "$RESULTSPATH/$TBL")
   fi
else
  echoerr "Bad exit status: $ESTAT"
  track_item current_operation "BaseRecalibrator-ErrorState"
  exit 100
fi

export BCALog=$JOB_ID

$DIR/stage2a/markCompleteBCAL.sh
