#!/bin/bash -x

source $DIR/lib/common_functions.sh

while getopts di:r:b:o:c: option
do
   case "$option" in
      d) DODEBUG=true;;             # option -d to print out commands
      i) INPUTBAM="$OPTARG";;
      r) REGIONS="$OPTARG";;        # option -r required target regions from RealignTargetCreator
      b) BQSR="$OPTARG";;           # optional -b for base recalibartion table from BaseRecalibrator
      o) OUTBAM="$OPTARG";;
      c) CHR="$OPTARG";;
   esac
done

trap "error-to-otto IndelRealigner-failed-$NM;track_item current_operation IndelRealigner-ErrorState;exit 100" SIGSTOP SIGUSR1 SIGUSR2 SIGHUP SIGINT SIGTERM ERR

[[ -z "$INPUTBAM" ]] && echo "Missing \$INPUTBAM" && exit 100
[[ ! -s "$INPUTBAM" ]] && echo "$INPUTBAM does not exist" && exit 100
[[ -z "$OUTBAM" ]] && echo "Missing \$OUTBAM" && exit 100
[[ -z "$REGIONS" ]] && echo "Missing \$REGIONS" && exit 100
[[ -z "$TMP_DIR" ]] && echo "Missing \$TMP_DIR" && exit 100
[[ -z "$KNOWN" ]] && echo "Missing \$KNOWN" && exit 100

NM=$(basename $INPUTBAM)
NM=${NM%%.*}

if [ ! -z "$BQSR" ];then
 BARGS="-BQSR $BQSR"
fi

if [ "$CHR" == "other" ];then
 L=$(seq -f"-XL chr%.0f" -s' ' 1 22)
 L+=" -XL chrX -XL chrY -XL chrM -XL unmapped"
else
 L="-L $CHR"
fi

SRT=$(date +%s)
NOW=$(date '+%F+%T')
track_item current_operation "IndelRealigner-$CHR"

java -Xmx12g -Djava.io.tmpdir=$TMP_DIR_R \
     -jar $GATK \
       -T IndelRealigner\
       -R $REF_FASTA \
       -I "$INPUTBAM" \
        -known $KNOWN \
        -known $DBSNP \
        -known $GOLD \
        -targetIntervals $REGIONS\
        $BARGS\
        $L\
        --bam_compression 1\
        -kpr\
        -SQQ 10 -SQQ 20 -SQQ 30 -SQQ 40\
        --disable_indel_quals\
        --preserve_qscores_less_than 6\
        -o "$OUTBAM"

echo $?

END=$(date +%s)
TM=$[$END-$SRT];
NOW=$(date '+%F+%T')

echo $TM
track_item current_operation "IndelRealigner-$CHR-exit"
