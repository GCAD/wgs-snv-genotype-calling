#!/bin/bash -x

source $DIR/lib/common_functions.sh

while getopts di:t:o: option
do
   case "$option" in
      d) DODEBUG=true;;               # option -d to print out commands
      i) INPUTBAM="$OPTARG";;
      o) OUTFILE="$OPTARG";;
      t) THREADS="$OPTARG";;
   esac
done

trap "error-to-otto RTC-failed-$NM;track_item current_operation RealignerTargetCreator-ErrorState;exit 100" SIGSTOP SIGUSR1 SIGUSR2 SIGHUP SIGINT SIGTERM ERR

[[ -z "$INPUTBAM" ]] && echo "Missing \$INPUTBAM" && exit 100
[[ ! -s "$INPUTBAM" ]] && echo "$INPUTBAM does not exist" && exit 100
[[ -z "$OUTFILE" ]] && echo "Missing \$OUTFILE" && exit 100
[[ -z "$TMP_DIR" ]] && echo "Missing \$TMP_DIR" && exit 100
[[ -z "$KNOWN" ]] && echo "Missing \$KNOWN" && exit 100
[[ -z "$THREADS" ]] && echo "Missing \$THREADS" && THREADS=$MAX_THREADS

NM=$(basename $INPUTBAM)
NM=${NM%%.*}

SRT=$(date +%s)
NOW=$(date '+%F+%T')
track_item rtc_start_tm $NOW
track_item current_operation "RealignerTargetCreator-$THREADS"

java -Djava.io.tmpdir=$TMP_DIR \
     -jar $GATK \
           -T RealignerTargetCreator\
           -R $REF_FASTA \
           -I $INPUTBAM \
           -nt $THREADS  \
           --known $KNOWN \
           --known $DBSNP \
           --known $GOLD \
           -o "$OUTFILE"

ESTAT=$?

END=$(date +%s)
TM=$[$END-$SRT];
NOW=$(date '+%F+%T')

track_item rtc_duration $TM
track_item rtc_done_tm $NOW

if [ -z "$RESULTSPATH" ];then
   export RESULTSPATH=$(get_s3_results_location $PRJ_ID $SM)
fi
if [ ! -z "$RESULTSPATH" ];then
   TBL=$(basename $OUTFILE)

   gzip $OUTFILE

   aws s3 cp --storage-class STANDARD_IA "$OUTFILE.gz" $RESULTSPATH/$TBL.gz
   S3TPATH=$(rawurlencode "$RESULTSPATH/$TBL.gz")
   track_item rtc_s3_path $S3TPATH

   gunzip $OUTFILE.gz
fi
