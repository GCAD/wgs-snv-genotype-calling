#!/bin/bash -x

source $DIR/lib/common_functions.sh
source $DIR/lib/seqfile_functions.sh

while getopts dt:h:p:o: option
do
   case "$option" in
      d) DODEBUG=true;;               # option -d to print out commands
      t) THREADS=$OPTARG;;
      h) HJID="-hold_jid $OPTARG";;    # job id/name for hold_jid
      p) PREFIX="$OPTARG";;
      o) OUTFILE="$OPTARG";;
   esac
done

[[ -z "$DIR" ]] && echo "Missing \$DIR" && exit 100
[[ -z "$PREFIX" ]] && echo "Missing \$PREFIX" && exit 100
[[ -z "$OUTFILE" ]] && echo "Missing \$OUTFILE" && exit 101
[[ -z "$SAMBAMBA" ]] && echo "Missing \$SAMBAMBA" && exit 102

COMPRESSION=9
NM=$(basename $PREFIX)
NM=${NM%%.*}

FNAME=$(basename "$OUTFILE")

[[ -z "$THREADS" ]] && THREADS=$MAX_THREADS

# make string containing all BAMs
MSTR=""
for CHR in `seq 1 22` X Y M -other -unmapped;do
  MSTR+="$PREFIX.chr$CHR.bam "
  if [ ! -s "$PREFIX.chr$CHR.bam" ];then
    echoerr "Missing input file $PREFIX.chr$CHR.bam"
    track_item current_operation "printReads-merge-ErrorState"
    exit 100
  fi
done

NOW=$(date '+%F+%T')
SRT=$(date +%s)

track_item gatk_br_done_time $NOW
track_item current_operation "printReads-merge"

trap "error-to-otto printReads-merge-failed-$NM;track_item current_operation printReads-Merge-ErrorState;exit 100" SIGSTOP SIGUSR1 SIGUSR2 SIGHUP SIGINT SIGTERM ERR

$SAMBAMBA merge --nthreads=$THREADS --compression-level=$COMPRESSION $OUTFILE $MSTR

ESTAT=$?

END=$(date +%s)
TM=$[$END-$SRT]

NOW=$(date '+%F+%T')
echo "Completed in $TM secs, $NOW"
track_item gatk_bqsr_done_time $NOW
track_item merge_sambamba_duration $TM
track_item bam_recal_size $(stat -c '%s' $OUTFILE)

if [ "$ESTAT" != 0 ];then
  echo "Bad exit status: $ESTAT"
  track_item current_operation "printReads-merge-ErrorState"
  exit 100
fi

[[ -z "$RESULTSPATH" ]] && export RESULTSPATH=$(get_s3_results_location $PRJ_ID $SM)

if [ ! -z $RESULTSPATH ];then

    echo "Make md5"
    md5sum $OUTFILE > $OUTFILE.md5 \
    && track_item current_operation "MarkDup-Merge-MD5" \
    && aws s3 cp --storage-class STANDARD_IA  $OUTFILE.md5 $RESULTSPATH/${FNAME/.bam/.md5}

    echo "Make flagstat"
    $SAMBAMBA flagstat ${OUTFILE} > ${OUTFILE/.bam/.flagstat} \
    && track_item current_operation "MarkDup-Merge-flagstat" \
    && aws s3 cp --storage-class STANDARD_IA  ${OUTFILE/.bam/.flagstat} $RESULTSPATH/${FNAME/.bam/.flagstat}

    echo "Begin upload"
    track_item current_operation "printReads-merge-upload"
    aws s3 cp --only-show-errors --storage-class STANDARD_IA  $OUTFILE $RESULTSPATH/$FNAME
    aws s3 cp --storage-class STANDARD_IA  $OUTFILE.bai $RESULTSPATH/$FNAME.bai


    if [ $(aws s3 ls $RESULTSPATH/$FNAME | grep -c .bam) == 0 ];then
       echoerr "bam not uploaded"
       error-to-otto "printReads-merge failed upload $NM"
       track_item current_operation "printReads-merge-Upload-ErrorState"
       exit 100
    fi


    S3BAMPATH=$(rawurlencode "$RESULTSPATH/$FNAME")
    [[ ! -z "$S3BAMPATH" ]] && track_item bam_recal_path $S3BAMPATH

    track_item bqsr_rg_cnt $(readgroup_cnt $OUTFILE)
    track_item bqsr_bam_size $(stat -c '%s' $OUTFILE)
    track_item bqsr_bam_md5 $(cut -f1 $OUTFILE.md5)

    for i in ${MSTR[@]};do
      rm -f "$i" "${i/.bam/.bai}"
    done
else
  echo "Missing RESULTSPATH, nothing uploaded"
fi

exit $ESTAT
