#!/bin/bash -x

source $DIR/lib/common_functions.sh
source $DIR/lib/seqfile_functions.sh

while getopts dt:h:p:o: option
do
   case "$option" in
      d) DODEBUG=true;;               # option -d to print out commands
      t) THREADS=$OPTARG;;
      h) HJID="-hold_jid $OPTARG";;    # job id/name for hold_jid
      p) PREFIX="$OPTARG";;
      o) OUTFILE="$OPTARG";;
   esac
done

trap "error-to-otto IndelRealigner-merge-failed-$NM;track_item current_operation IndelRealigner-Merge-ErrorState;exit 100" SIGSTOP SIGUSR1 SIGUSR2 SIGHUP SIGINT SIGTERM ERR

[[ -z "$DIR" ]] && echo "Missing \$DIR" && exit 100
[[ -z "$PREFIX" ]] && echo "Missing \$PREFIX" && exit 100
[[ -z "$OUTFILE" ]] && echo "Missing \$OUTFILE" && exit 100
[[ -z "$THREADS" ]] && THREADS=$MAX_THREADS
[[ -z "$SAMBAMBA" ]] && echo "Missing \$SAMBAMBA" && exit 100

COMPRESSION=9
NM=$(basename $PREFIX)
NM=${NM%%.*}

FNAME=$(basename "$OUTFILE")
CRAMFILE=${OUTFILE/.bam/.cram}

# make string containing all BAMs
MSTR=""
for CHR in `seq -f"chr%0.0f" -s' ' 1 22` chrX chrY chrM other unmapped;do
  MSTR+="$PREFIX.$CHR.bam "
  if [ ! -s "$PREFIX.$CHR.bai" ];then
    echoerr "Missing input file $PREFIX.$CHR.bam"
    error-to-otto IndelRealigner-merge-failed-$NM;
    track_item current_operation IndelRealigner-Merge-ErrorState;
    exit 100
  fi
done

NOW=$(date '+%F+%T')
SRT=$(date +%s)

track_item indelRealign_done_time $NOW
track_item current_operation "IndelRealigner-merge"

# Increase open files limit
ulimit -Hn 65536
ulimit -Sn 65536
ulimit

time $SAMBAMBA merge --nthreads=$THREADS --compression-level=$COMPRESSION $OUTFILE $MSTR
time $SAMBAMBA index "$OUTFILE"
time $SAMTOOLS view -C -T $REF_FASTA -@$THREADS -o $CRAMFILE $OUTFILE #should just use sambamba (uses htslib, just like samtools)
time $SAMBAMBA index -C $CRAMFILE

ESTAT=$?

END=$(date +%s)
TM=$[$END-$SRT]

NOW=$(date '+%F+%T')
echo "Completed in $TM secs, $NOW"
track_item indelRealign_merge_done_time $NOW
track_item indelRealign_merge_duration $TM
track_item indelRealign_bam_size $(stat -c '%s' $OUTFILE)

#delete temp files used in making merged file
for i in ${MSTR[@]};do
 rm -f $i ${i/.bam/.bai}
done

[[ -z "$RESULTSPATH" ]] && export RESULTSPATH=$(get_s3_results_location $PRJ_ID $SM)

if [ ! -z $RESULTSPATH ];then

    echo "Make md5"
    track_item current_operation "IndelRealigner-merge-MD5"
    md5sum $OUTFILE > $OUTFILE.md5
    aws s3 cp $OUTFILE.md5 $RESULTSPATH/${FNAME/.bam/.md5}

    md5sum $CRAMFILE > $CRAMFILE.md5
    aws s3 cp $CRAMFILE.md5 $RESULTSPATH/

    echo "Make flagstat"
    track_item current_operation "IndelRealigner-merge-flagstat"
    FLAGSTAT=${OUTFILE/.bam/.flagstat}
    $SAMBAMBA flagstat ${OUTFILE} > "$FLAGSTAT"
    aws s3 cp "$FLAGSTAT" $RESULTSPATH/${FNAME/.bam/.flagstat}

    echo "Begin upload"
    track_item current_operation "IndelRealigner-merge-upload"
    aws s3 cp --only-show-errors --storage-class STANDARD_IA  $OUTFILE $RESULTSPATH/$FNAME
    aws s3 cp $OUTFILE.bai $RESULTSPATH/$FNAME.bai
    aws s3 cp --only-show-errors --storage-class STANDARD_IA  $CRAMFILE $RESULTSPATH/
    aws s3 cp $CRAMFILE.crai $RESULTSPATH/

    if [ $(aws s3 ls $RESULTSPATH/$FNAME | grep -c .bam) == 0 ];then
       echoerr "bam not uploaded"
       error-to-otto "Merge failed upload $NM"
       track_item current_operation "Merge-Upload-ErrorState"
       exit 100
    fi

    S3BAMPATH=$(rawurlencode "$RESULTSPATH/$FNAME")
    [[ ! -z "$S3BAMPATH" ]] && track_item bam_recal_path $S3BAMPATH

    track_item bqsr_rg_cnt $(readgroup_cnt $OUTFILE)
    track_item bqsr_bam_size $(stat -c '%s' $OUTFILE)
    track_item bqsr_bam_md5 $(cut -f1 $OUTFILE.md5)

    track_item bqsr_cram_size $(stat -c '%s' $CRAMFILE)
    track_item bqsr_cram_md5 $(cut -f1 $CRAMFILE.md5)

     # Extract flagstat stats
     TOT=$(grep "in total"     $FLAGSTAT | grep -Po "^\d+")
     DUPS=$(grep "duplicates"  $FLAGSTAT | grep -Po "^\d+")
     MAPPED=$(grep "mapped ("  $FLAGSTAT | grep -Po "^\d+")
     SINGLE=$(grep "singletons" $FLAGSTAT | grep -Po "^\d+")
     PAIRED=$(grep "properly paired" $FLAGSTAT | grep -Po "^\d+")

     DPCENT=$(echo $DUPS   | awk -v TOTAL=$TOT '{printf "%3.2f", ($1/TOTAL*100)}')
     MPCENT=$(echo $MAPPED | awk -v TOTAL=$TOT '{printf "%3.2f", ($1/TOTAL*100)}')
     PPCENT=$(echo $PAIRED | awk -v TOTAL=$TOT '{printf "%3.2f", ($1/TOTAL*100)}')
     SPCENT=$(echo $SINGLE | awk -v TOTAL=$TOT '{printf "%3.2f", ($1/TOTAL*100)}')

     # Upload flagstat stats
     track_item bqsr_ir_reads $TOT
     track_item bqsr_ir_dups $DUPS
     track_item bqsr_ir_pcent $DPCENT
     track_item bqsr_ir_mapped_reads $MAPPED
     track_item bqsr_ir_mapped_pcent $MPCENT
     track_item bqsr_ir_paired_reads $PAIRED
     track_item bqsr_ir_paired_pcent $PPCENT
     track_item bqsr_ir_singletons   $SINGLE
     track_item bqsr_ir_singletons_pcent $SPCENT


    for i in ${MSTR[@]};do
      rm -f "$i" "${i/.bam/.bai}"
    done
else
  echo "Missing RESULTSPATH, nothing uploaded"
fi

exit $ESTAT
