#!/bin/bash -x
#
#
# @param SEQFILE - full path to BAM/CRAM
#

source pipeline.ini
source lib/common_functions.sh
source lib/seqfile_functions.sh

export SEQFILE=$1 # full bam/cram path
[[ -z "$SEQFILE" ]] && echo "Missing \$SEQFILE" && exit 100
[[ -z "$BAM_DIR" ]] && echo "Missing \$BAM_DIR" && exit 101
[[ -z "$MAX_THREADS" ]] && echo "Missing \$MAX_THREADS" && exit 102
[[ -z "$SAMTOOLS" ]] && echo "Missing \$SAMTOOLS" && exit 103

# count number of original quality
RGCT=$(readgroup_cnt $SEQFILE)

# Get read groups as array
RGRPS=($(get_readgroups $SEQFILE))

#
# submitBAM2BWA
# seq map BAM/CRAM using bwa
# @param SEQFILE - full path to BAM/CRAM
function submitBAM2BWA {
 local RGBAM=$1
 local HOLD=$2

 local NM=$(basename $RGBAM)
 NM=${NM/.bam/}
 NM=${NM/.sorted-byname/}

 local RGCT=$(readgroup_cnt $SEQFILE)
 if [ $RGCT -gt 1 ];then
   TOTMEM=$(free -g|grep Mem|awk '{print $2}')
 else
   TOTMEM=95
 fi

 local THREADS=$[ $MAX_THREADS / ($TOTMEM / 95) ]
 local MEM=$(adjustWorkingMem 95G $THREADS)

 qsub \
    -N BAM2BWA-$NM \
    -cwd \
    -V \
    -hold_jid "$HOLD" \
    -j y \
    -notify \
    -pe DJ $THREADS \
    -o $LOGS/ \
    -l h_vmem=$MEM \
      $DIR/stage0/streamBAM2bwa.sh "$RGBAM" $THREADS
}

make_new_tmp

for RG in ${RGRPS[@]};do
  echo "Procssing RG:$RG"
# step 1: sort/split-by-rg
# output BAM files as ${RG}.sorted-byname.bam
  export THREADS=$[ $MAX_THREADS / $RGCT ]
  if [ "$THREADS" == "0" ];then export THREADS=1;fi

  if [ ! -s "$TMP_DIR/$RG.sorted-byname.bam" ];then
    qsub \
      -N bamba-sort-flt-$RG \
      -cwd  \
      -o $LOGS/ \
      -pe DJ $THREADS \
      -l h_vmem=5G \
      -V \
      -j y $DIR/stage0/readname-sort.sh $SEQFILE $RG
  fi

  submitBAM2BWA "$TMP_DIR/$RG.sorted-byname.bam" "bamba-sort-flt-$RG"

done
